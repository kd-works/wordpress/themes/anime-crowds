</div><!-- #main -->
</section><!-- #wrapper-inner -->
</section><!-- #wrapper -->

<footer id="footer">
	<div id="footer-widget">
	
	<?php
		if (   ! is_active_sidebar( 'first-footer-widget-area'  )
			&& ! is_active_sidebar( 'second-footer-widget-area' )
			&& ! is_active_sidebar( 'third-footer-widget-area'  )
			&& ! is_active_sidebar( 'fourth-footer-widget-area' )
		) :
	?>
	<div id="first" class="widget-area">
		<ul class="xoxo">
			<li id="links" class="widget-container">
				<h3 class="widget-title noel-lock">.:: <?php _e( 'Affiliated Sites', 'noel' ); ?></h3>
				<ul>
					<li><a href="http://mimic-project.com" target="_blank">MIMIC-project</a></li>
					<li><a href="http://mimic-project.com/anime-crowds" target="_blank">Demo Page</a></li>
					<li><a href="http://zero.intikali.org" target="_blank">Persona Intikalia</a></li>
					<li><a href="http://iskael.com" target="_blank">Iskael</a></li>
					<li><a href="http://wordpress.org" target="_blank">WordPress</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div id="second" class="widget-area">
		<ul class="xoxo">
			<li id="links" class="widget-container">
				<h3 class="widget-title noel-lock">.:: <?php _e( 'Official Channels', 'noel' ); ?></h3>
				<ul>
					<li><a href="https://www.facebook.com/pages/MIMIC-Project/119527808221085" target="_blank">Facebook</a></li>
					<li><a href="https://twitter.com/mimic_project" target="_blank">Twitter</a></li>
					<li><a href="https://plus.google.com/118015446664769570303" target="_blank">Google +</a></li>
				</ul>
			</li>
		</ul>
	</div>
	<div id="third" class="widget-area" style="width:350px;margin:10px 50px;">
		<ul class="xoxo">
			<li id="text" class="widget-container">
				<p>"Thank you for using our product. You can find more nifty stuffs from MIMIC-Project's official site at <a style="text-decoration:underline;" href="http://mimic-project.com" title="MIMIC-Project Official Site" target="_blank">http://mimic-project.com</a>."</p>
			</li>
			<li id="search" class="widget-container widget_search">
				<?php get_search_form(); ?>
			</li>
		</ul>
	</div>
	<?php endif; ?>
	
	<?php if ( is_active_sidebar( 'first-footer-widget-area' ) ) : ?>
				<div id="first" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'first-footer-widget-area' ); ?>
					</ul>
				</div><!-- #first .widget-area -->
	<?php endif; ?>	
	<?php if ( is_active_sidebar( 'second-footer-widget-area' ) ) : ?>
				<div id="second" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'second-footer-widget-area' ); ?>
					</ul>
				</div><!-- #second .widget-area -->
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'third-footer-widget-area' ) ) : ?>
				<div id="third" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'third-footer-widget-area' ); ?>
					</ul>
				</div><!-- #third .widget-area -->
	<?php endif; ?>
	<?php if ( is_active_sidebar( 'fourth-footer-widget-area' ) ) : ?>
				<div id="fourth" class="widget-area">
					<ul class="xoxo">
						<?php dynamic_sidebar( 'fourth-footer-widget-area' ); ?>
					</ul>
				</div><!-- #fourth .widget-area -->
	<?php endif; ?>
			
	</div><!-- #footer-widget -->
	<?php noel_credit(); ?>
</footer><!-- #footer -->

<?php wp_footer(); ?>
</BODY>
</HTML>